# Курсовой проект

##  Структура проекта

Проект состоит из 4 проектов в GitLab:

* [Создание кластера k8s в GCP](https://gitlab.com/scdf-k8s-project/infra/-/blob/master/README.md)
* [Развертываение Spring Cloud Data Flow ](https://gitlab.com/scdf-k8s-project/scdf-platform/-/blob/master/README.md)
* [Тестовое приложение демонстрирующее Spring Cloud Data Flow ](https://gitlab.com/scdf-k8s-project/scdf-platform/-/blob/master/README.md)
* [Проект с документацией ](https://gitlab.com/scdf-k8s-project/scdf-platform/-/blob/master/README.md)
  
![](img/pipeline.png)


---  
##  Шаги развертывания и настройки компонентов

### *Развертывание инфраструктуры*

Для развертывания  платформы необходимо выполнить несколько шагов:
* Проверить [конфигурацию развертыванию кластера K8S](https://gitlab.com/scdf-k8s-project/infra#%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B3%D1%83%D1%80%D0%B0%D1%86%D0%B8%D1%8F)
![](img/infra_vars.png)


* Запустить [пайплайн развертывания](https://gitlab.com/scdf-k8s-project/infra/-/pipelines/new).
* Конечные этапы пайплайна по созданию и разрушению кластера - ручные

![](img/pipeline.png)

### *Развертывание платформы Spring Cloud Data Flow*

* Проверить [конфигурацию развертыванию платформы Spring Cloud Data Flow](https://gitlab.com/scdf-k8s-project/scdf-platform#%D1%80%D0%B0%D0%B7%D0%B2%D0%B5%D1%80%D1%82%D1%8B%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5)
![](img/scdf_vars.png)


* Запустить [пайплайн развертывания](https://gitlab.com/scdf-k8s-project/scdf-platform/-/pipelines/new).
* Конечные этапы пайплайна по созданию и разрушению кластера - ручные

![](img/pipeline.png)

После развертывания можно  получить адреса для входа в приложение
![](img/ingresses.png)


### *Развертывание тестового приложения в Spring Cloud Data Flow*

для развертывания приложения внутрь платформы Spring Cloud Data Flow необходимо выполнить несколько шагов:
* Проверить [конфигурацию ](https://gitlab.com/scdf-k8s-project/infra#%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B3%D1%83%D1%80%D0%B0%D1%86%D0%B8%D1%8F)
![](img/task_vars.png)
* Запустить [пайплайн развертывания](https://gitlab.com/scdf-k8s-project/task_app/-/pipelines/new), уствановив параметр dataflow_server  URL  сервера

Проверить, что приложение развернулось можно в дашборде Spring Cloud Data Flow
![](img/task_deployed.png)
---
## Описание интеграции с CI/CD-сервисом

* Весь исходный код храниться под системой контроля версий в gitlab
* Запуск создания и\или обновления компонетов системы производится через запуск на исполнение pipeline проекта
* Создание инфраструктуры в GCP, в качестве бекенда использует облачное объектное хранилище
* Развертывание инфраструктурных сервисов и платформы для приложений, осуществляется в пайплайне Gitlab, и может быть развернуто на любом кластере k8s. Для тестирования инфраструктуры на этапе разработки используется  minikube
* Для тестового приложения написан  pipeline сборки и развертывания. Развертывается только сборки протегирование по семверу.
## Подход к организации мониторинга и логирования
Мониторинг обеспечивается развертываением следующего стека:
* Prometheus Proxy - PushGateWay для подов с тасками и job
* Prometheus Server - сервер сбора и  хранения метрик
* Grafana - средство визуализации собранных метрик

Все компоненты, развертываются как инфраструктурные  сервисы в пайплайне, и доступны через Ingress.

* [grafana](https://gitlab.com/scdf-k8s-project/scdf-platform/-/blob/master/grafana.tf)
* [prometheus](https://gitlab.com/scdf-k8s-project/scdf-platform/-/blob/master/grafana.tf)

В развертывание включено автоматическое конфигурирование источников данных и Dashboard для платформы  Spring Cloud Data Flow и Kubernetes Pods.

Логирование реализуется через [развертыванием элементов](https://gitlab.com/scdf-k8s-project/scdf-platform/-/blob/master/loki.tf) стека [Loki](https://grafana.com/oss/loki/), в составе 
* сервера Loki
* агента сбора логов Promtail

Все компоненты, развертываются как инфраструктурные  сервисы в пайплайне, и доступны через Ingress.
  

## Дополнительная информация про вашу платформу
* [Changelog](Changelog.md)
* [Checklist](k8s-checklist.md)
* [Spring Cloud Data Flow](https://spring.io/projects/spring-cloud-dataflow)
* [ETL с потоком данных Spring Cloud](https://www.codeflow.site/ru/article/spring-cloud-data-flow-etl)
* [Getting Started with Spring Cloud Data Flow and Confluent Cloud](https://www.confluent.io/blog/apache-kafka-spring-cloud-data-flow-tutorial/)